## Setup

1. Install [Nitrogen](http://nitrogenproject.com/)

        git clone https://github.com/nitrogen/nitrogen

1. Create a new nitrogen app

        cd nitrogen
        make rel_cowboy PROJECT=<newapp>

1. Checkout site on top of new app

        cd ../<newapp>
        rm -rf site
        git clone git@bitbucket.org:afrantisak/<newapp>.git site

1. Build

        make

1. Run

        bin/nitrogen console

